<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package theme-by-socreativ
 */

get_header();
?>

	<main id="primary" class="site-main" data-barba="container" data-barba-namespace="terms-archive">

		<a href="<?= get_post_type_archive_link('intervention'); ?>" class="back-btn <?php if(!my_wp_is_mobile()) echo 'back-btn-w'; ?>">
		<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><title>ic_arrow_back_24px</title>
			<g fill="#fcfcfc" class="nc-icon-wrapper">
				<path d="M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z"></path>
			</g>
		</svg>
		Retour</a>

		<div class="archive-content">
			<div class="left-col container">
				<?php 
					if ( have_posts() ) {
						$i = 0;
						while ( have_posts() ) {
							$i++;
							$index = $i < 10 ? str_pad($i, 2, '0', STR_PAD_LEFT)  : $i;
							the_post();
							get_template_part( 'template-parts/content', get_post_type(), array('index' => $index));
						}
					}
				?>
				<div class="space-bottom" style="height: 15vh"></div>
			</div>
			<h1 class="term__title"><?= get_queried_object()->name ?></h1>
			<?php $thumb = null;
				if(have_posts()){
					while(have_posts() && $thumb === null){
						the_post();
						$thumb = get_the_post_thumbnail_url();
					}
				}		
			?>
			
			<?php if(!my_wp_is_mobile()): ?>
			<div class="right-col">
				<img id="intervention-thumbnail" src="<?= $thumb; ?>" alt="">
			</div>
			<?php endif; ?>
		</div>



	</main><!-- #main -->

	
<?php get_footer(); ?>
