<a class="intervention__link" href="<?= get_post_permalink() ?>" thumb-src="<?= get_the_post_thumbnail_url(); ?>">
    <div class="intervention-link <?php if(my_wp_is_mobile()){?>d-flex<?php }?>">
        <?php if(my_wp_is_mobile()){?>
                <img class="img-inter-mobile" src="<?= get_the_post_thumbnail_url(); ?>" alt="<?php the_title() ?>">
            <div class="p-3">
        <?php }?>
        <p class="intervention__index"><?= $args['index']; ?></p>
        <h2 class="intervention__title"><?php the_title() ?></h2>
        <?php if(my_wp_is_mobile()){?>
            </div>
        <?php }?>
    </div>
</a>