deffered['presentation'].resolve(PresentationInit);

function PresentationInit() {

    const element = document.querySelector('.image-container')
    if (element) {
        if (window.innerWidth < 576) {
            gsap.to(element.querySelector('.presentation-image > div.scale'), {
                y: -150, ease: Power2.easeInOut, scrollTrigger: {
                    trigger: element,
                    start: 'top top',
                    end: 'bottom top',
                    scrub: true,
                }
            });
        } else {
            gsap.to(element.querySelector('.presentation-image > div.scale'), {
                opacity: 0, y: '0vh', ease: Power2.easeInOut, scrollTrigger: {
                    trigger: '.presentation-container',
                    start: 'top top',
                    end: 'bottom top',
                    scrub: true,
                }
            });
        }

        const presentations = document.querySelectorAll('.presentation-container');
        presentations.forEach((presentation) => {
            let st = ScrollTrigger.create({
                trigger: presentation,
                start: "top top+=100px",
                end: "bottom top",
                onEnter: () => document.body.classList.add("white-fill"),
                onLeave: () => document.body.classList.remove("white-fill"),
                onEnterBack: () => document.body.classList.add("white-fill"),
                onLeaveBack: () => document.body.classList.remove("white-fill"),
            });
        });
    }

}
