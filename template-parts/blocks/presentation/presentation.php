<section <?php if (isset($block['anchor'])) echo 'id="' . $block['anchor'] . '"' ; ?> class="<?php if (isset($block['className'])) echo $block['className']; ?>">
    <?php
    $big_title = get_field('big_title');
    $texte_1 = get_field('texte_1');
    $soustitre = get_field('soustitre');
    $signature = get_field('signature');
    $scroll = get_field('scroll');
    $titre_presentation = get_field('titre_presentation');
    $texte_presentation = get_field('texte_presentation');
    $image = get_field('image');
    ?>
    <div class="container-fluid presentation-container">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6 full-height">
                    <div class="d-flex flex-column justify-content-evenly ">
                        <h1 class="title mb-4 ">   <?= $big_title ?>
                        </h1>
                        <p class="texte_1 mb-4">
                            <?= $texte_1 ?>
                        </p>
                        <p class="soustitre mb-4">
                            <?= $soustitre ?>
                        </p>
                        

                        <?php if(!my_wp_is_mobile()): ?>
                        <div class="scroll-indicator">
                            <div class="gold-el"></div>
                            <div class="fixed-el"></div>
                            <p class="scroll-indicator-text">scroll</p>
                        </div>
                        <?php endif; ?>

                    </div>
                    <div class="mt-md-5 pt-md-5">
                        <h3 class="titre_presentation mb-5 ">
                            <?= $titre_presentation; ?>
                        </h3>
                        <p class="texte_presentation">
                            <?= $texte_presentation ?>
                        </p>
                        <img class="mt-4 mb-4" style="max-width:160px;" src="<?= $signature['url']; ?>" alt="<?= $signature['alt']; ?>">
                        <div class="presentation-row row justify-content-between h-100">
                            <?php if (have_rows('coordonnees')): ?>
                            <?php while (have_rows('coordonnees')):
                            the_row(); ?>
                                            <?php
                            $chiffres = get_sub_field('chiffres');
                            $texte_1 = get_sub_field('texte_1');
                            $texte_2 = get_sub_field('texte_2');
                            ?>
                            <div class="col col-lg-6">
                                <div>
                                    <p class="mb-5 fw-600 text-gold">
                                        <?=$chiffres?>
                                    </p>
                                    <p>
                                        <?=$texte_1?>
                                    </p>
                                </div>
                            </div>
                            <?php
                                endwhile; ?>
                            <?php
                                endif; ?>
                        </div>
                    </div>
                </div>
                <div class="image-container col col-lg-6">
                    <div class="presentation-image">
                        <div class="mask-img-presentation"></div>
                        <div class="h-100 w-100 position-relative scale">
                            <img class="" src="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



</section>