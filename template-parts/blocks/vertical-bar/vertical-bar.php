<?php

if(my_wp_is_mobile()){
    $mt = get_field('mt_mobile');
    $mb = get_field('mb_mobile');
    $h = get_field('height_mobile');
}else{
    $mt = get_field('mt');
    $mb = get_field('mb');
    $h = get_field('height');
}


?>

<div <?php if(isset($block['anchor'])) echo 'id="'.$block['anchor'].'"'; ?> class="vertical-bar" style="height:<?php echo $h ?>vh;margin-top:<?php echo $mt ?>vh;margin-bottom:<?php echo $mb ?>vh;">
    <div class="animed"></div>
    <div class="fixed"></div>
</div>
