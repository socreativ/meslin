"use strict";

deffered['cabinet'].resolve(CabinetInit);


function CabinetInit() {

    GetBlockAssets('simple-slider', true).then(() => {
        if(!callback['simple-slider']) callback['simple-slider'] = new Promise((res, rej) => deffered[block] = { resolve: res, reject: rej });
    });
    callback['simple-slider'].then((res) => res());

    const element = document.querySelector('.cabinet-container')

    gsap.to(element.querySelector('.initiales-img'), {
        y: '-10vh', ease: Power2.easeInOut, scrollTrigger: {
            trigger: element,
            start: 'top bottom',
            end: 'bottom top',
            scrub: true,
        }
    });
    gsap.to(element.querySelector('.cabinet-image img'), {
        scale: '1.2', ease: Power2.easeInOut, scrollTrigger: {
            trigger: element,
            start: 'top bottom',
            end: 'bottom top',
            scrub: true,
        }
    });

}