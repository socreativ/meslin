

<section <?php if (isset($block['anchor'])) echo 'id="' . $block['anchor'] . '"'; ?> class=" <?php if (isset($block['className'])) echo $block['className']; ?>">
<?php
$image = get_field('image');
$initiales = get_field('initiales');
$titre = get_field('titre');
$texte = get_field('texte');
$lien = get_field('lien');
?>
<div class="container-fluid p-0">
    <div class="container cabinet-container d-flex justify-content-between">
        <div class="row col col-lg-6 position-relative">
        <div class="cabinet-image">
            <?php include get_template_directory() . "/template-parts/blocks/simple-slider/simple-slider.php"; ?>
        </div>
            <img src="<?=$initiales['url'] ?>" class="position-absolute initiales-img" alt="<?=$initiales['alt'] ?>">
        </div>
        <div class="cabinet-row row justify-content-between col col-lg-5 h-100">
            <div class="">
                <div>
                    <h2 class="mb-4 cabinet-title">
                        <?=$titre?>
                    </h2>
                </div>
                <div class="cabinet-texte">
                    <p>
                        <?=$texte?>
                    </p>
                </div>
                <?php  if($lien): ?>
                <a href="<?= $lien['url'] ?>" class="light-btn position-absolute">
                    <?= $lien['title'] ?>
                </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
   


</section>
