"use strict";

deffered['homepage'].resolve(HomeInit);

function HomeInit(){
    const element = document.querySelector('.idk')
    if(window.innerWidth > 576){
        gsap.to(element, {opacity: 0, scale:1.3, y:'50vh', ease: Power2.easeInOut, scrollTrigger: {
            trigger: '.homepage',
            start: 'top top',
            end: 'bottom top',
            scrub: true,
            //markers : true, 
        }});
    }
}
