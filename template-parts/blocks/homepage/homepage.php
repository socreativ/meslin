<?php
# Pitié monsieur, pas le tiroir à code nul
# Get ACF
# slider options
$type = get_field('my_slider_type');
// $speed = get_field('param')['speed'];
// $anim_speed = get_field('param')['anim_speed'];

# images
$isDiapo = get_field('isSingle');
$images = get_field('images_diapo');
$image_single = get_field('image_single');

# title
$is_single_title = !get_field('single_title');
if($is_single_title){
  $title = get_field('title')['text'];
  $title_css = get_field('title')['css'];
  $title_el = get_field('title')['balise'];
  $title_attr = get_field('title')['attr'];
}else{
  $titles = get_field('multiple_titles')['titles'];
  $title_el = get_field('multiple_titles')['balise'];
  $title_css = get_field('multiple_titles')['css'];
}

# subtitle
$is_single_subtitle = !get_field('single_subtitle');
if($is_single_subtitle){
  $subtitle = get_field('subtitle')['text'];
  $subtitle_css = get_field('subtitle')['css'];
  $subtitle_el = get_field('subtitle')['balise'];
  $subtitle_attr = get_field('subtitle')['attr'];
}else{
  $subtitles = get_field('multiple_subtitles')['subtitles'];
  $subtitle_el = get_field('multiple_subtitles')['balise'];
  $subtitle_css = get_field('multiple_subtitles')['css'];
}

# short text
$short = get_field('short');

?>

<!-- SLIDER BEGIN -->
<section <?php if(isset($block['anchor'])) echo 'id="'.$block['anchor'].'"'; ?> class="homepage appear-section <?php if(isset($block['className'])) echo $block['className']; ?>">
    <?php if(!my_wp_is_mobile()): ?>
      <?php   if(!$type): ?>

        <?php if(!$isDiapo): ?>
            <img class="idk" src="<?php echo $image_single['sizes']['large'] ?>" alt="<?php echo $image_single['alt'] ?>"/>
        <?php else:
          if($images && !my_wp_is_mobile()): ?>
    
          <div class="slider-container">

            <div class="main-slider" speed="<?php echo $speed ?>" anim-speed="<?php echo $anim_speed ?>">

          <?php foreach ($images as $single): ?>
              <img class="main-slider-bkg" data-skip-lazy="" src="<?= esc_url($single['url']) ?>" data-lazy="<?php echo esc_url($single['url']) ?>" alt="<?php echo $single['alt'] ?>">
          <?php  endforeach; ?>
            </div>
          </div>
        <?php else:?>
          <img src="<?php echo $images[0]['sizes']['medium']; ?>" alt="<?php echo $images[0]['alt'] ?>"/>

        <?php endif;
        endif; ?>
          <div class="greyscale"></div>
      <?php endif; ?>

    <?php else : ?>
    <?php endif; ?>
    
      <div class="slider-content">
        <?php  if(my_wp_is_mobile()): ?>
          <img class="idk" src="<?php echo $image_single['sizes']['large'] ?>" alt="<?php echo $image_single['alt'] ?>"/>
        <?php endif; ?>
        <div></div>
        <div class="slider-text appear">
        <?php if($is_single_title): #single title?>
              <?php if($title_el): ?>
                <<?php echo $title_el; ?> class="<?php echo $title_css; ?>" <?php echo $title_attr; ?>><?php echo $title; ?></<?php echo $title_el; ?>>
              <?php endif; ?>
        <?php else: #title > 1 ?>
            <?php if(have_rows('multiple_titles')): while(have_rows('multiple_titles')): the_row(); ?>
              <?php if(have_rows('titles')): $i=0; ?>
                <<?php echo $title_el; ?> class="<?php echo $title_css; ?>">
                  <?php while(have_rows('titles')): the_row(); $i++; #foreach titles ?>
                    <?php if($i!=1) echo '<br>'; ?><?php if(get_sub_field('title')['css']) echo '<span class="'. get_sub_field('title')['css'] . '">'; ?><?php echo get_sub_field('title')['text'];; ?><?php if(get_sub_field('title')['css']) echo '</span>'; ?>
                  <?php endwhile; ?>
                </<?php echo $title_el; ?>>
              <?php endif; ?>
            <?php endwhile; endif; ?>
        <?php endif; ?>

        <?php if($is_single_subtitle): #single subtitle?>
              <?php if($subtitle_el): ?>
                <<?php echo $subtitle_el; ?> class="<?php echo $subtitle_css; ?>" <?php echo $subtitle_attr; ?>><?php echo $subtitle; ?></<?php echo $subtitle_el; ?>>
              <?php endif; ?>
        <?php else: #subtitle > 1 ?>
        <?php if(have_rows('multiple_subtitles')): while(have_rows('multiple_subtitles')): the_row(); ?>
              <?php if(have_rows('subtitles')): $i=0; ?>
                <<?php echo $subtitle_el; ?> class="<?php echo $subtitle_css; ?>">
                  <?php while(have_rows('subtitles')): the_row(); $i++; #foreach subtitles ?>
                    <?php if($i!=1) echo '<br>'; ?><?php if(get_sub_field('subtitle')['css']) echo '<span class="'. get_sub_field('subtitle')['css'] . '">'; ?><?php echo get_sub_field('subtitle')['text'];; ?><?php if(get_sub_field('subtitle')['css']) echo '</span>'; ?>
                  <?php endwhile; ?>
                </<?php echo $subtitle_el; ?>>
              <?php endif; ?>
            <?php endwhile; endif; ?>
        <?php endif; ?>
        <?php if(my_wp_is_mobile()): ?>
          <a class="gold-btn rdv-btn-home-mobile d-inline-flex" style="color: white!important;" href="<?= get_permalink(763); ?>">PRENDRE RDV</a>
        <?php endif; ?>
        </div>

        <div class="slider-bottom">
          <div class="interventions-slider">
              <h3>Interventions</h3>
              <div class="inter-list">
                <?php foreach (get_field('interventions') as $id => $term): ?>
                  <a href="<?= get_term_link($term->term_id)  ?>"><?= $term->name; ?></a>
                <?php endforeach; ?> 
                <a href="<?= get_post_type_archive_link('intervention'); ?>" class="all-inter"><span>+</span></a>
              </div>
          </div>


          <div class="slider-buttons">
            <?php if(have_rows('btn')):
              while(have_rows('btn')): the_row();
                  $link = get_sub_field('link');
                  $css = get_sub_field('css_class'); ?>
                  <a href="<?php echo $link['url']; ?>" class="<?php echo $css; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
              <?php endwhile;
            endif; ?>
          </div>

        </div>
     

      </div>


    <?php wp_reset_query(); ?>

    <?php if(!my_wp_is_mobile()): ?>
      <div class="scroll-indicator">
        <div class="gold-el"></div>
        <div class="fixed-el"></div>
        <p class="scroll-indicator-text">scroll</p>
      </div>
    <?php endif; ?>


</section>
<!-- END SLIDER -->
