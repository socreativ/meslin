"use strict";


function conventionInit(){
    const element = document.querySelector('.coordonnees-container .coordonnees-image')
if(window.innerWidth > 576){
        gsap.to(element.querySelector('.coordonnees-image img'), {scale:'1.2',y: '0vh', ease: Power2.easeInOut, scrollTrigger: {
            trigger: element,
            start: 'top bottom',
            end: 'bottom top',
            scrub: true,
            //markers: true,
        }});
    }
}

function InitconventionCallback(){
    callback['convention'] = conventionInit;
    callback['convention']();
}

docReady(() => {
    InitconventionCallback();
});
