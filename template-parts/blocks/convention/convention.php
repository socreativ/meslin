<section <?php if (isset($block['anchor'])) echo 'id="' . $block['anchor'] . '"'; ?> class="convention-block <?php if (isset($block['className'])) echo $block['className']; ?>">

<div class="p-0 container-fluid position-relative">
    <div class="container coordonnees-container d-flex justify-content-between">
        <div class="coordonnees-row row justify-content-between col col-lg-6 h-100">
    <?php if (have_rows('coordonnees')): ?>
        <?php while (have_rows('coordonnees')):
        the_row(); ?>
        <?php
        $chiffres = get_sub_field('chiffres');
        $texte_1 = get_sub_field('texte_1');
        $texte_2 = get_sub_field('texte_2');
        ?>
            <div class="col-12 col-lg-6">
                <div>
                    <p class="mt-5 mt-md-0 mb-md-5 fw-600"><?=$chiffres?></p>
                    <p><?=$texte_1?></p>
                </div>
                <div class="text_2">
                    <p><?=$texte_2?></p>
                </div>
            </div>
        <?php
    endwhile; ?>
        <?php
endif; ?>
        </div>
        <?php $img = get_field('image') ?>
        <div class="row col-12 col-lg-4 position-static">
            <div class="coordonnees-image position-absolute overflow-hidden">
                <img src="<?=$img['url'] ?>" class="" alt="<?=$img['alt'] ?>">
            </div>
        </div>
    </div>
</div>
   


</section>
