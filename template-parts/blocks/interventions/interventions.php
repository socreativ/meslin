<section <?php if(isset($block['anchor'])) echo 'id="'.$block['anchor'].'"'; ?> class="interventions <?php if(isset($block['className'])) echo $block['className']; ?>">

    <?php $terms = get_terms(array('taxonomy' => 'categorie', 'hide_empty' => false)); ?>

    <div class="intervention-grid">
        <?php foreach ($terms as $index => $term): ?>
            <a href="<?= get_term_link($term->term_id); ?>"><div class="inter-card">
                <div class="filter"></div>
                <img src="<?= get_field('cat_img', $term)['sizes']['square'] ?>" alt="" class="inter-bkg">
                <h5><?= $term->name ?></h5>
                <p class="inter-nbr"><?= $term->count ?> <?php echo $term->count === 1 ? 'intervention' : 'interventions'; ?></p>
                <p class="inter-more"><span>+</span></p>
            </div></a>
        <?php endforeach; ?>
    </div>

</section>

