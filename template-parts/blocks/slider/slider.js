"use strict";

deffered['slider'].resolve(SliderInit);

function SliderInit() {

    document.querySelectorAll('.slider').forEach(s => {
        const slider = new SoSlider(s, {
            speed: 350,
            arrows: true,
            infinite: true,
            dots: true,
            slideToScroll: 1,
            slideToShow: 1
        });
    });
}
