<section <?php if(isset($block['anchor'])) echo 'id="'.$block['anchor'].'"'; ?> class="slider-block <?php if(isset($block['className'])) echo $block['className']; ?>">
    <div class="slider">
        <?php $galerie = get_field('galerie'); ?>
        <?php foreach($galerie as $image): ?>
            <img data-fancybox="beforeAfter" src="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>">
        <?php endforeach; ?>
    </div>
</section>