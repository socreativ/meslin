function SpinMeRound(){
    const spins = document.querySelectorAll('.spin-round');
    if(spins.length !== 0){
        spins.forEach(e => {
           gsap.to(e.querySelector('img'), {rotation: 360, ease: Power2.easeInOut, scrollTrigger: {
                trigger: e,
                start: "top-=20vh bottom",
                end: "bottom-=20vh top",
                scrub: true,
            }});
            if(window.innerWidth > 576){
                gsap.to(e, {y: "-30vh", scrollTrigger: {
                    trigger: e,
                    start: "top bottom",
                    end: "bottom top",
                    scrub: true
                }});
            }
        });
    }
    else{
        console.warn('No spins found : make sure function is called on the right page');
    }
}


function InitSpinCallback(){
    callback['spin'] = SpinMeRound;
}

docReady(() => {
    InitSpinCallback();
});
