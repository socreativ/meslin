<section <?php if(isset($block['anchor'])) echo 'id="'.$block['anchor'].'"'; ?> class="spin-round <?php if(isset($block['className'])) echo $block['className']; ?>">
    <div class="dots-circle">
        <div class="blur-circle">
            <img src="<?= get_field('content')['url']; ?>" alt="<?= get_field('content')['alt']; ?>">
            <div class="inner-circle"></div>
        </div>
    </div>
</section>

