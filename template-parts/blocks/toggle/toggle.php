<section <?php if(isset($block['anchor'])) echo 'id="'.$block['anchor'].'"'; ?> class="image-toggle <?php if(isset($block['className'])) echo $block['className']; ?>">

    <h2 class="toggle__title"><?= get_field('title'); ?></h2>

    <div class=" toggle__selectors">
        <?php if(have_rows('content')): ?>
            <?php while(have_rows('content')): the_row(); ?>
            <?php $images = get_sub_field('Images'); ?>
            <div class="toggle__selectors_container">
                <a href="##" data-src="<?= $images['url']; ?>" class="toggle__selectors_select <?php if(get_row_index() == 1) echo 'selected' ?>"><?= get_sub_field('name'); ?></a>
            </div>
            <?php endwhile; ?>
        <?php endif; ?>
    </div>

    <div class="toggle__frame">
      <img src="<?= get_field('content')[0]['Images']['url']; ?>" id="toggle_images" alt="">
    </div>


</section>