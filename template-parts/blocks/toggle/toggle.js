"use strict";

deffered['toggle'].resolve(toggleImage);

function toggleImage(){
    let anim = null;
    let img = document.getElementById('toggle_images');
    let imageLink = Array.from(document.querySelectorAll('.toggle__selectors_select'));
    imageLink.forEach((t, i) => {
        t.addEventListener("click", () => {
            clearTimeout(anim)
            img.animate([{opacity: 0},],{duration: 350, fill: "forwards", easing: 'ease-out'});
            removeClass(imageLink);
            t.classList.toggle('selected');
            let data = t.getAttribute('data-src');
            anim = setTimeout(() => {
                img.src = data;
                img.animate([{opacity: 1},],{duration: 350, fill: "forwards", easing: 'ease-out'});
            }, 350);
        });
    })
}

function removeClass(link) {
    link.forEach(element => {
        element.classList.remove('selected');
    });
}




