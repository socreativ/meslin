function ProfilInit(){

    const element = document.querySelector('.gloubiboulga')

    gsap.to(element.querySelector('.back'), {y: 150, ease: Power2.easeInOut, scrollTrigger: {
        trigger: element,
        start: 'top-=300 center',
        end: 'bottom+=300 center',
        scrub: true,
    }});

    gsap.to(element.querySelector('.front'), {y: -150, ease: Power2.easeInOut, scrollTrigger: {
        trigger: element,
        start: 'top-=300 center',
        end: 'bottom+=300 center',
        scrub: true,
    }});
    
}

function InitProfilCallback(){
    callback['profil'] = ProfilInit;
    callback['profil']();
}

docReady(() => {
    InitProfilCallback();
});
