<section <?php if(isset($block['anchor'])) echo 'id="'.$block['anchor'].'"'; ?> class="profil <?php if(isset($block['className'])) echo $block['className']; ?>">

    <div class="title-resize">
        <h2 class="big-title resize"><?= get_field('titre'); ?></h2>
    </div>

    <div class="gloubiboulga">
        <div class="back">
            <div class="corner"></div>
        </div>
        <img class="profil-pic" src="<?= get_field('image')['url'] ?>" alt="<?= get_field('image')['alt'] ?>">
        <!-- <div class="gradient"></div> -->
        <div class="front"></div>
    </div>
    
</section>