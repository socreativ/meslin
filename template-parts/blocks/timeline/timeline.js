deffered['timeline'].resolve(Timeline);

function Timeline(){
    const timelines = document.querySelectorAll('.timeline');
    if(timelines.length !== 0){
        const timeline_key = document.querySelectorAll('.timeline__key');
        timelines.forEach(timeline => {
            const timeline_bar = timeline.querySelector('.timeline__bar_progress');
            gsap.to(timeline_bar, {scaleY: 1, ease: Power0.easeNone , scrollTrigger: {
                trigger: timeline,
                start: "top-=100 center",
                end: "bottom+=150 center",
                scrub: true
            }});
            const dots = timeline.querySelectorAll('.timeline__dot');
            dots.forEach((dot, i) => {
                ScrollTrigger.create({
                    trigger: dot,
                    scrub: true,
                    start: "center center+=25",
                    end: "center center+=25",
                    onEnter: () => {
                        dot.classList.add('reached');
                        timeline_key[i].classList.add('visible');
                    },
                    onEnterBack: () => {
                        dot.classList.remove('reached');
                        timeline_key[i].classList.remove('visible');
                    }
                });
            });
        });
    }
    else{
        console.warn('No timeline found : ')
    }
}