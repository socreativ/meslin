<section <?php if(isset($block['anchor'])) echo 'id="'.$block['anchor'].'"'; ?> class="timeline <?php if(isset($block['className'])) echo $block['className']; ?>">

    <div class="timeline__container">
        <?php if(have_rows('dates')): ?>
            <?php while(have_rows('dates')): the_row(); ?>
            <?php $css = get_row_index() % 2 == 0 ? "right-side" : "left-side" ?>
                <div class="timeline__key <?= $css ?>">
                    <p class="key__year"><?= get_sub_field('annee'); ?></p>
                    <p class="key__title"><?= get_sub_field('title'); ?></p>
                    <p class="key_desc"><?= get_sub_field('desc'); ?></p>
                </div>
            <?php endwhile ?>
        <?php endif; ?>
            <?php if(!my_wp_is_mobile()){ ?>
                <div class="timeline__dots">
                    <?php if(have_rows('dates')): ?>
                    <?php while(have_rows('dates')): the_row(); ?>
                    <span class="timeline__dot"></span>
                    <?php endwhile ?>
                    <?php endif; ?>
                </div>
                <span class="timeline__bar"><span class="timeline__bar_top"></span><span class="timeline__bar_progress"></span></span>
            <?php } ?>
    </div>


</section>

