<div class="intervention__single">

    <div class="single__content">

        <div class="content__header">
            <p class="single__categorie">Catégorie : <?= wp_get_post_terms(get_queried_object()->ID, 'categorie')[0]->name ?></p>
            <h1 class="single__title"><?= get_the_title(); ?></h1>
        </div>

        <?= get_the_post_thumbnail(); ?>

        <div class="content__scroll">
            <?php the_content(); ?>
        </div>

    </div>

    <div class="single__sidebar">

        <?php if(my_wp_is_mobile()): ?>
            <div class="single__sidebar_tab">
                <span class="tab_dot"></span>
                <span class="tab_dot"></span>
                <span class="tab_dot"></span>
            </div>
        <?php endif; ?>

        <div class="single__duration">
            <p>
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 48 48"><title>ic_access_time_48px</title>
                    <g fill="#0000002C" class="nc-icon-wrapper">
                        <path d="M23.99 4C12.94 4 4 12.95 4 24s8.94 20 19.99 20C35.04 44 44 35.05 44 24S35.04 4 23.99 4zM24 40c-8.84 0-16-7.16-16-16S15.16 8 24 8s16 7.16 16 16-7.16 16-16 16zm1-26h-3v12l10.49 6.3L34 29.84l-9-5.34z"></path>
                    </g>
                </svg>
                <?php _e('Durée', 'socreativ-theme'); ?>
                <span><?= get_field('duration')['duration']; ?></span>
            </p>

            <?php if(get_field('duration')['text']): ?>
                <p class="mt-3 fs-12"><?= get_field('duration')['text']; ?></p>
            <?php endif; ?>
        </div>

        <div class="single__price">
            <p>
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 48 48"><title>ic_euro_symbol_48px</title>
                    <g fill="#0000002C" class="nc-icon-wrapper">
                        <path d="M30 37c-5.01 0-9.36-2.84-11.53-7H30v-4H17.17c-.1-.65-.17-1.32-.17-2s.07-1.35.17-2H30v-4H18.47c2.17-4.16 6.51-7 11.53-7 3.23 0 6.18 1.18 8.45 3.13L42 10.6C38.82 7.75 34.61 6 30 6c-7.83 0-14.48 5.01-16.95 12H6v4h6.12c-.08.66-.12 1.32-.12 2 0 .68.04 1.34.12 2H6v4h7.05c2.47 6.99 9.12 12 16.95 12 4.61 0 8.82-1.75 12-4.6l-3.55-3.54C36.18 35.81 33.23 37 30 37z"></path>
                    </g>
                </svg>
                <?php _e('À partir de ', 'socreativ-theme'); ?>
                <span><?= get_field('price')['price']; ?></span>
            </p>
        </div>

        <div class="single__anchors">
            <div class="single__tracker"></div>

            <?php if(have_rows('anchors')): ?>
                <?php while(have_rows('anchors')): the_row(); ?>
                    <a href="<?= get_sub_field('anchor')['url'] ?>"><?= get_sub_field('anchor')['title'] ?></a>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>

        <p class="single__sommaire">Sommaire</p>

        <div class="single__sidebar_contact text-white">
            <a class="gold-btn" href="<?= get_field('lien_prefooter', 'options')['url']; ?>"><?= get_field('lien_prefooter', 'options')['title'] ?></a>
        </div>

    </div>


</div>
