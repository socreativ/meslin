# Benoît Meslin

### Particularité du site

La particularité de ce site par rapport aux autres projets réalisé par l'agence est l'utilisation de **[BarbaJS](https://barba.js.org/)**, une librairie javascript permettant de gérer les transitions de page en y ajoutant des animations. <br>
Le fonctionnement est assez simple : lors d'un hover sur un lien, la prochaine page est déjà pré-chargé, puis lors du clic le DOM de la future page est ajouter au DOM actuel, une animation de sortie s'applique au DOM de l'ancienne page, puis en suivant l'animation d'entrée s'applique sur le nouveau DOM pour enfin supprimer le DOM de l'ancienne page. <br>
En fin de compte on ne change réellement jamais de page, ce sont seulement les DOM qui sont échangé et actualisé. Dans le cas des sites Wordpress que nous réalisons nous devons donc gérer dans le contrôleur d'animation le chargement des blocs Gutenberg et l'appel des fonctions Javascript associés. 

Ici sur ce site nous ne changeons pas la totalités du DOM mais seulement les éléments `<main>` ce qui signifie que nous gardons les même header, footer et éléments fixes tout au long de la navigations que nous allons simplement altérer via des classes lorsque nécessaire. 

## GetBlockAssets & CreateAssets

Il existe deux fonctions ([**GetBlockAssets**](./src/js/script.js#script.js-112) et [**CreateAssets**](./src/js/script.js#script.js-131)) qui permettent de chargé les assets (CSS & Javascript) lié à un block. 

La fonction **GetBlockAssets** est appelé depuis le contrôleur d'animation en y passant en paramètre le nom de block, et un booléen optionnel si un fichier requiert un fichier Javascript ou non. Cela va vérifier si le block à déjà était chargé ou non et si ce n'est pas le cas,  via une [**fonction**](./functions.php#functions.php-500) AJAX cela va charger les URL des fichier css et js correspondant au block puis les envoyer à la fonction **CreateAssets** qui va simplement créer une balise `<link>` ou `<script>` avec l'url du blocks correspondant.

## Callback

Lors du chargement d'une nouvelle page il faut donc relancer chaque fonctions de chaque block se trouvant normalement présente dans le DocReady tel que les EventListener, scrollTriggers etc... Pour ce faire on ajoute ces fonctions dans un tableau *callback* définis dans le [script](./src/js/script.js) principal et accessible dans tout les autres fichiers. Ce tableau contient donc toutes les fonctions vitales de chaque blocks et peuvent donc être appelée depuis le contrôleur de transition au chargement d'un page si nécessaire.

## Contrôleur de transition

Le fichier [transition.js](./src/js/transition.js) contient donc le contrôleur des transition initialisé directement grâce à la méthode `barba.init()` à laquelle on passe un objet en paramètre définissant le comportement des transition. <br>

L'objet paramètre est séparer en deux parties principales. 
 - ***transitions***: définis l'animation utilisé en entrée, sortie, au chargement, ainsi que des paramètres supplémentaire (name, sync, etc...)
 - ***views***: Cela permet de définir des cas particulier en fonction d'une page, ou d'une namespace et surchargé les paramètres standard comme l'animation ou les hook du [cycle de vie](https://barba.js.org/docs/getstarted/lifecycle/) comme *`afterEnter`* ou *`beforeEnter`*. Ce son dans ces fonctions que l'ont va appeler nos fonctions pour charger les blocks nécessaire à la page et appeler les fonctions vitales des blocks.

L'idée étant de charger les blocks avant l'affichage de la page donc *`beforeEnter`* et une fois chargé appelé les fonction dans le *`afterEnter`*.

On peut avoir besoin de changer des états, des classes, supprimez des éléments statique spécifique à la page que l'on s'apprête à quitter. C'est à l'aide du hook  *`afterLeave`* que l'on va pouvoir effectuer ces actions. 

Plus bas dans le fichier nous avons déclarer des fonctions à exécuter dans les hooks à chaque transitions, des fonctions comme pour remonter le scroll en haut de page, rafraichir les scrollTrigger, ou kill les instances scrollTrigger ou slider plus nécessaire.

### Notes:

Le site reposent énormément sur les [**Promesse**](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise) Javascript car toutes les animations sont gérer en asynchrone.