<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package theme-by-socreativ
 */

get_header();

$ac = my_wp_is_mobile() ? '#FFF' : '#C5AB9E'; 

?>

	<main id="primary" class="site-main" data-barba="container" data-barba-namespace="interventions">

		<a href="<?= get_term_link(wp_get_post_terms(get_queried_object()->ID, 'categorie')[0]->term_id) ?>" class="back-btn">
		<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><title>ic_arrow_back_24px</title>
			<g fill="<?= $ac ?>" class="nc-icon-wrapper">
				<path d="M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z"></path>
			</g>
		</svg>
		Retour</a>

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content-single', get_post_type() );

		endwhile; // End of the loop.
		?>

	</main><!-- #main -->

<?php
get_footer();
