<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package theme-by-socreativ
 */

wp_enqueue_style('block-acf-interventions', get_stylesheet_directory_uri() . '/template-parts/blocks/interventions/interventions.css', true, '1.0', 'all');

get_header();
?>

	<main id="primary" class="site-main" data-barba="container" data-barba-namespace="archive-intervention">


        <a href="<?= get_permalink(556); ?>" class="see-charter light-btn">VOIR LA CHARTE</a>

        <?php include_once get_template_directory() . '/template-parts/blocks/interventions/interventions.php';  ?>

	</main><!-- #main -->

<?php
get_footer();
