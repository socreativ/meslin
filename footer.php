<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package theme-by-socreativ
 */

?>

<footer id="colophon" class="site-footer">

    <div class="footer__logo">
        <?= get_field('footer_logo', 'options'); ?>
    </div>

    <a class="transparent-btn footer_link" href="<?= get_field('lien_prefooter', 'options')['url']; ?>">
        <?= get_field('lien_prefooter', 'options')['title'] ?>
    </a>
    <div class="col-6 d-flex flex-column align-items-end h-100">
        <div class="d-flex align-items-center mx-auto justy-content-center">
            <?php if( have_rows('reseau_sociaux_menu','options') ): ?>
                <ul class="list-unstyled d-flex p-3 mt-3">
                    <?php while( have_rows('reseau_sociaux_menu','options') ): the_row(); ?>
                    <li>
                        <?php 
                        $link = get_sub_field('lien');
                        if( $link ): 
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                            ?>
                            <a class="button p-2 m-2" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                                <?php 
                                    $image = get_sub_field('logo');
                                    if( !empty( $image ) ): ?>
                                        <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                                    <?php endif;
                                ?>
                            </a>
                        <?php endif; ?>
                    </li>
                <?php endwhile; ?>
                </ul>
            <?php endif; ?>
        </div>
    </div>

    <div class="footer__content">
        <?= get_field('last_p', 'options') ?>

    </div>

    <div class="footer__mentions">
        <?php dynamic_sidebar('footer'); ?>
    </div>

</footer><!-- #colophon -->


<?php wp_footer(); ?>

</body>
</html>
