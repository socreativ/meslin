<?php 
if ( ! function_exists( 'create_post_type' ) ) :

function create_post_type() {

    /*********************************************
    * 
    *              Interventions
    *
    **********************************************/

	$labels = array(
        'name' => __('Interventions'),
        'all_items' => __('Toutes les interventions'),
        'singular_name' => __('Intervention'),
        'add_new_item' => __('Ajouter une intervention'),
        'edit_item' => __('Modifier l\'intervention'),
        'menu_name' => __('Interventions')
    );
	
	$args = array(
        'labels'                => $labels,
        'public'                => true,
        'hierarchical'          => false,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'show_in_rest'          => true,
        'capability_type'       => 'post',
		'supports'              => array ( 'title', 'excerpt', 'editor', 'custom-fields', 'page-attributes', 'thumbnail', 'revisions' ),
        'menu_position'         => 31, 
        'menu_icon'             => 'dashicons-portfolio',
	);

	register_post_type( 'intervention', $args);


    /*********************************************
    * 
    *             Taxonomies
    *
    **********************************************/

    // Localisation
	$labels = array(
		'name' => __('Catégorie'),
		'new_item_name' => __('Nom de la nouvelle catégorie'),
		'parent_item' => __('Type de catégorie parente'),
	);

	$args = array( 
		'labels' => $labels,
		'public' => true, 
		'show_in_rest' => true,
		'hierarchical' => true, 
		'has_archive' => true,
		'show_admin_column' => true
	);

	register_taxonomy( 'categorie', 'intervention', $args );


}
add_action( 'init', 'create_post_type' );



endif;