<?php 

    // Check function exists.
    if( function_exists('acf_register_block_type') ) {

         // Espacement
         acf_register_block_type(array(
            'name'              => 'spacing',
            'title'             => __('Espacement'),
            'description'       => __('margin block'),
            'category'          => 'custom-blocks',
            'icon'              => 'editor-break',
            'keywords'          => array( 'margin', 'space', 'vh'),
            'render_template'   => '/template-parts/blocks/spacing/spacing.php',
            'supports'          => array( 'anchor' => true),
        ));
        acf_register_block_type(array(
            'name'              => 'vertical-bar',
            'title'             => __('Vertical Bar'),
            'description'       => __('Vertical Bar'),
            'render_template'   => 'template-parts/blocks/vertical-bar/vertical-bar.php',
            'category'          => 'custom-blocks',
            'icon'              => 'tide',
            'keywords'          => array( 'Vertical bar', 'margin', 'vertical'),
            'supports'          => array( 'anchor' => true),
            'enqueue_script' => get_template_directory_uri() . '/template-parts/blocks/vertical-bar/vertical-bar.js',
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/vertical-bar/vertical-bar.css',
        ));

        acf_register_block_type(array(
            'name'              => 'homepage',
            'title'             => __('Home'),
            'description'       => __('Home block for Dr. Meslin'),
            'render_template'   => 'template-parts/blocks/homepage/homepage.php',
            'category'          => 'custom-blocks',
            'icon'              => 'format-gallery',
            'keywords'          => array( 'slider', 'video', 'picture'),
            'supports'          => array( 'anchor' => true),
            'enqueue_script' => get_template_directory_uri() . '/template-parts/blocks/homepage/homepage.js',
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/homepage/homepage.css',
        ));

 
        acf_register_block_type(array(
            'name'              => 'slider',
            'title'             => __('Slider'),
            'description'       => __('Slider'),
            'render_template'   => 'template-parts/blocks/slider/slider.php',
            'category'          => 'custom-blocks',
            'icon'              => 'format-gallery',
            'keywords'          => array( 'slider', 'video', 'picture'),
            'supports'          => array( 'anchor' => true),
            'enqueue_script' => get_template_directory_uri() . '/template-parts/blocks/slider/slider.js',
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/slider/slider.css',
        ));

        acf_register_block_type(array(
            'name'              => 'block-agrandissable',
            'title'             => __('block-agrandissable'),
            'description'       => __('block agrandissable'),
            'category'          => 'custom-blocks',
            'icon'              => 'admin-comments',
            'keywords'          => array( 'relatedpost', 'quote' ),
            'render_template'   => 'template-parts/blocks/block-agrandissable/content-block-agrandissable.php',
            'supports'          => array( 'anchor' => true),
        ));

        acf_register_block_type(array(
            'name'              => 'interventions',
            'title'             => __('Interventions'),
            'description'       => __('block interventions'),
            'category'          => 'custom-blocks',
            'icon'              => 'portfolio',
            'keywords'          => array( 'relatedpost', 'quote' ),
            'render_template'   => 'template-parts/blocks/interventions/interventions.php',
            'supports'          => array( 'anchor' => true),
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/interventions/interventions.css',
        ));

        acf_register_block_type(array(
            'name'              => 'scroll-spin',
            'title'             => __('Scroll Spin'),
            'description'       => __('You spin me right round, baby right round, like a record baby, right round, \'round \'round'),
            'category'          => 'custom-blocks',
            'icon'              => 'image-rotate',
            'keywords'          => array( 'spin', 'beyblade' ),
            'render_template'   => 'template-parts/blocks/scroll-spin/scroll-spin.php',
            'supports'          => array( 'anchor' => true),
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/scroll-spin/scroll-spin.css',
            'enqueue_script' => get_template_directory_uri() . '/template-parts/blocks/scroll-spin/scroll-spin.js',
        ));

        acf_register_block_type(array(
            'name'              => 'timeline',
            'title'             => __('Timeline'),
            'description'       => __('Timeline progressing with scroll'),
            'category'          => 'custom-blocks',
            'icon'              => 'calendar-alt',
            'keywords'          => array( 'Timeline', 'key', 'date', 'event'),
            'render_template'   => 'template-parts/blocks/timeline/timeline.php',
            'supports'          => array( 'anchor' => true),
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/timeline/timeline.css',
            'enqueue_script' => get_template_directory_uri() . '/template-parts/blocks/timeline/timeline.js',
        ));

        acf_register_block_type(array(
            'name'              => 'toggle',
            'title'             => __('Toggle Image'),
            'description'       => __('Toggle Image'),
            'category'          => 'custom-blocks',
            'icon'              => 'align-full-width',
            'keywords'          => array( 'Toggle', 'Click', 'Image', 'List', 'Category'),
            'render_template'   => 'template-parts/blocks/toggle/toggle.php',
            'supports'          => array( 'anchor' => true),
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/toggle/toggle.css',
            'enqueue_script' => get_template_directory_uri() . '/template-parts/blocks/toggle/toggle.js',
        ));

        acf_register_block_type(array(
            'name'              => 'presentation',
            'title'             => __('presentation'),
            'description'       => __('presentation'),
            'category'          => 'custom-blocks',
            'icon'              => 'align-full-width',
            'keywords'          => array( 'presentation', 'Click', 'Image', 'List', 'Category'),
            'render_template'   => 'template-parts/blocks/presentation/presentation.php',
            'supports'          => array( 'anchor' => true),
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/presentation/presentation.css',
            'enqueue_script' => get_template_directory_uri() . '/template-parts/blocks/presentation/presentation.js',
        ));

        acf_register_block_type(array(
            'name'              => 'convention',
            'title'             => __('Convention Info'),
            'description'       => __('Convention Info'),
            'category'          => 'custom-blocks',
            'icon'              => 'align-full-width',
            'keywords'          => array( 'Convention', 'Click', 'Info', 'List', 'Category'),
            'render_template'   => 'template-parts/blocks/convention/convention.php',
            'supports'          => array( 'anchor' => true),
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/convention/convention.css',
            'enqueue_script' => get_template_directory_uri() . '/template-parts/blocks/convention/convention.js',
        ));

        acf_register_block_type(array(
            'name'              => 'cabinet',
            'title'             => __('Cabinet BM'),
            'description'       => __('Cabinet BM'),
            'category'          => 'custom-blocks',
            'icon'              => 'align-full-width',
            'keywords'          => array( 'Cabinet', 'Click', 'Info', 'List', 'Category'),
            'render_template'   => 'template-parts/blocks/cabinet/cabinet.php',
            'supports'          => array( 'anchor' => true),
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/cabinet/cabinet.css',
            'enqueue_script' => get_template_directory_uri() . '/template-parts/blocks/cabinet/cabinet.js',
        ));

        acf_register_block_type(array(
            'name'              => 'simple-slider',
            'title'             => __('simple-slider BM'),
            'description'       => __('simple-slider BM'),
            'category'          => 'custom-blocks',
            'icon'              => 'align-full-width',
            'keywords'          => array( 'simple-slider', 'Click', 'Info', 'List', 'Category'),
            'render_template'   => 'template-parts/blocks/simple-slider/simple-slider.php',
            'supports'          => array( 'anchor' => true),
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/simple-slider/simple-slider.css',
            'enqueue_script' => get_template_directory_uri() . '/template-parts/blocks/simple-slider/simple-slider.js',
        ));

        acf_register_block_type(array(
            'name'              => 'profil',
            'title'             => __('Section Profile'),
            'description'       => __('Section profile avec image & blur'),
            'category'          => 'custom-blocks',
            'icon'              => 'align-full-width',
            'keywords'          => array( 'Profil', 'Blur', 'Image'),
            'render_template'   => 'template-parts/blocks/profil/profil.php',
            'supports'          => array( 'anchor' => true),
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/profil/profil.css',
            'enqueue_script' => get_template_directory_uri() . '/template-parts/blocks/profil/profil.js',
        ));

        add_action('acf/init', 'register_acf_block_types');

    }


?>