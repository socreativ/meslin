<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package theme-by-socreativ
 */

 $bodyCss = isset($args['class']) ?: '';
 $bodyCss = my_wp_is_mobile() ? 'mobile' : 'desktop';
 #print_r($_SERVER['HTTP_USER_AGENT']);

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php echo get_field('head_scripts', 'options') ?>
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<?php the_field( "google_font","option" ); ?>
	<?php wp_head(); ?>
</head>
<body class="<?= $bodyCss ?>" data-barba="wrapper">

<?php wp_body_open(); ?>
<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'theme-by-socreativ' ); ?></a>

<header id="masthead" class="site-header">

	<div class="logo">
		<a href="<?= home_url() ?>"><?= get_field('logo', 'options')['svg']; ?></a>
	</div>



	<nav id="site-navigation" class="aaa">
		<div class="burger">			
			<div class="burger-cross position-relative">
				<span class="hamb-dots anim-300"></span>
				<span class="hamb-dots anim-300"></span>
				<span class="hamb-dots anim-300 m-0"></span>
				<span class="cross-line anim-300"></span>
				<span class="cross-line cross-line2 anim-300"></span>
			</div>
			<?php if(!my_wp_is_mobile()): ?><p class="burger-text-hor">menu</p><?php endif; ?>
			<?php if(!my_wp_is_mobile()): ?><p class="burger-text-ver">menu</p><?php endif; ?>
		</div>
		<div class="burger-nav">
			<div class="row">
				<div class="col col-md-6 d-flex flex-column h-100 justify-content-center">
					<div class="nav-intervention mb-2">
						<h3 class="mt-3 mt-md-5 mb-3">INTERVENTIONS</h3>
						<ul>			
							<?php
								$terms = get_terms(array('taxonomy' => 'categorie', 'hide_empty' => false));
								foreach ($terms as $k => $term):						?>
								<li class="nav-interventions d-inline-flex">
									<a href="<?= get_term_link($term->term_id); ?>" data-src="<?= get_field('cat_img', $term)['sizes']['square'] ?>">
										<span><?= str_pad($k+1, 2, '0', STR_PAD_LEFT); ?></span>
										<?= $term->name ?>
									</a>
								</li>
								<?php endforeach; ?>
							</ul>
						</div>
						<div class="d-flex flex-column justify-content-center">
							<a class="charte-btn-burger my-2 text-gold uppercase gold-link" href="<?= get_permalink(233); ?>"><?= get_the_title(233); ?></a>
							<a class="charte-btn-burger my-2 text-gold uppercase gold-link" href="<?= get_permalink(556); ?>"><?= get_the_title(556); ?></a>
						</div>
					<?php if(my_wp_is_mobile()): ?>
						<?php if( have_rows('reseau_sociaux_menu','options') ): ?>
							<ul class="mt-5 list-unstyled d-flex pr-3">
								<?php while( have_rows('reseau_sociaux_menu','options') ): the_row(); ?>
								<li>
									<?php 
									$link = get_sub_field('lien');
									if( $link ): 
										$link_url = $link['url'];
										$link_title = $link['title'];
										$link_target = $link['target'] ? $link['target'] : '_self';
										?>
										<a class="button p-2 mr-2" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
											<?php 
												$image = get_sub_field('logo');
												if( !empty( $image ) ): ?>
													<img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
												<?php endif;
											?>
										</a>
									<?php endif; ?>
								</li>
							<?php endwhile; ?>
							</ul>
						<?php endif; ?>
						<a class="gold-btn rdv-btn-burger" style="color: white!important;" href="<?= get_permalink(763); ?>">PRENDRE RDV</a>
						<?php endif; ?>
				</div>
				<?php if(!my_wp_is_mobile()): ?>
				<div class="col-6 d-flex flex-column align-items-end h-100">
					<img src="https://socreativ-host.com/meslin/wp-content/uploads/2021/11/cabinet-docteur-benoit-meslin.jpg"
					class="nav-img" alt="cabinet">
					<div class="d-flex align-items-center">
						<?php if( have_rows('reseau_sociaux_menu','options') ): ?>
							<ul class="list-unstyled d-flex pr-3">
								<?php while( have_rows('reseau_sociaux_menu','options') ): the_row(); ?>
								<li>
									<?php 
									$link = get_sub_field('lien');
									if( $link ): 
										$link_url = $link['url'];
										$link_title = $link['title'];
										$link_target = $link['target'] ? $link['target'] : '_self';
										?>
										<a class="button p-2 mr-2" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
											<?php 
												$image = get_sub_field('logo');
												if( !empty( $image ) ): ?>
													<img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
												<?php endif;
											?>
										</a>
									<?php endif; ?>
								</li>
							<?php endwhile; ?>
							</ul>
						<?php endif; ?>
						<a class="my-4 gold-btn" href="<?= get_permalink(763); ?>">PRENDRE RDV</a>
					</div>
				</div>
				<?php endif; ?>
			</div>
			<p id="low-title">Menu</p>
		</div>
	</nav>


</header>

