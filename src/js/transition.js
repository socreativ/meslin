barba.init({
  debug: false,
  transitions: [
    {
      sync: false,
      name: 'opacity-transition',
      async leave({ current }) {
        await FadeOut(current.container);
      },
      enter({ next }) {
        FadeIn(next.container);
      },
      once({ next }) {
        FadeIn(next.container);
      }
    }
  ],
  views: [
    {
      namespace: 'homepage',
      async afterEnter() {
        ListenForBigTitle();
        ScrollIndicator();
        callback['homepage'].then(res => res());
        callback['spin'].then(res => res());
        callback['profil'].then(res => res());
        callback['cabinet'].then(res => res());
        callback['convention'].then(res => res());
      },
      async beforeEnter() {
        GetBlockAssets('homepage', true);
        ShowFooter();
        GetBlockAssets('profil', true);
        GetBlockAssets('interventions');
        GetBlockAssets('scroll-spin', true);
        GetBlockAssets('cabinet', true);
        GetBlockAssets('convention', true);
        ToggleLockSticky(false);
      }
    },
    {
      namespace: 'terms-archive',
      afterEnter: () => {
        if (window.innerWidth > 768) InterventionLinkHover();
      },
      beforeEnter: () => {
        HideFooter();
        document.body.classList.add('tax-archive');
        ToggleLockSticky(true);
        ToggleInvertFilter(document.querySelector('#site-navigation'));
      },
      beforeLeave: () => {
        ToggleInvertFilter(document.querySelector('#site-navigation'));
        document.body.classList.remove('tax-archive');

      }
    },
    {
      namespace: 'interventions',
      afterEnter: () => {
        AnchorScrollSpy();
        callback['slider'].then(res => res());
      },
      beforeLeave: () => {
        document.body.classList.remove('single-intervention')
        removeScrollListener();
      },
      beforeEnter: () => {
        GetBlockAssets('slider', true);
        ToggleLockSticky(false);
        ListenForToggleSideBar();
        document.body.classList.add('single-intervention')
        ShowFooter();
      }
    },
    {
      namespace: 'archive-intervention',
      async beforeEnter() {
        GetBlockAssets('interventions');
        ToggleLockSticky(true);
        HideFooter();
      }
    },
    {
      namespace: 'presentation',
      async afterEnter() {
        callback['presentation'].then((res) => res());
        callback['timeline'].then((res) => res());
        callback['toggle'].then((res) => res());
        callback['cabinet'].then((res) => res());
        ListenForBigTitle();
        document.body.classList.add('white-fill');
      },
      async beforeEnter() {
        ScrollIndicator();
        ToggleLockSticky(false);
        ShowFooter();
        GetBlockAssets('presentation', true);
        GetBlockAssets('timeline', true);
        GetBlockAssets('cabinet', true);
        GetBlockAssets('toggle', true);
      },
      afterLeave: () => {
        setTimeout(() => document.body.classList.remove('white-fill'), 250);
      }
    },
    {
      namespace: 'contact',
      beforeEnter: () => {
        ShowFooter();
        ToggleLockSticky(true);

      },
      afterEnter: () => {

      }
    }
  ]
});

barba.hooks.afterLeave((data) => {
  window.scrollTo({ top: 0, behavior: 'instant' });
});

barba.hooks.afterEnter((data) => {
  ScrollTrigger.refresh();
  window.dispatchEvent(new Event("resize"));
});

barba.hooks.beforeEnter((data) => {
  if (document.body.classList.contains('nav-open')) ToggleNavVisibility();
  SoSlider.killAll();
  let st = ScrollTrigger.getAll();
  st.forEach(s => {
    s.kill();
  });
});


/*****************************
 *          Animations
 *****************************/

function FadeIn(e, d) {
  return new Promise(async resolve => {
    await gsap.to(e, { delay: d, duration: 0.75, opacity: 1 })
      .then(resolve());
  });
}

function FadeOut(e) {
  return new Promise(async resolve => {
    await gsap.to(e, { duration: 0.75, opacity: 0 });
    resolve();
  });
}