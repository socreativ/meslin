"use-strict";

const callback = [];
const deffered = [];

function docReady(fn) {
    if (document.readyState === "complete" || document.readyState === "interactive") {
        setTimeout(fn, 1);
    } else {
        document.addEventListener("DOMContentLoaded", fn);
    }
}

function ScrollIndicator(){
    const scrollIndicator = document.querySelectorAll('.scroll-indicator');
    if(scrollIndicator.length !== 0){
        scrollIndicator.forEach(e => {
            const el= e.querySelector('.gold-el');
            const tl = gsap.timeline({repeat: -1});
            tl.to(el, 1.5, {scaleY: 1, transformOrigin: 'center top'})
            .to(el, 1.5, {scaleY: 0, transformOrigin: 'center bottom'});
        });
    }
    else{
        console.warn('No ".scroll-indicator" elements found : make sure function is called on the right page.')
    }
}

function InterventionLinkHover(){
    const interventionLink = document.querySelectorAll('.intervention__link');
    if(interventionLink.length !== 0){
        let anim;
        const thumb = document.querySelector('#intervention-thumbnail');
        interventionLink.forEach(e => {
            e.addEventListener('mouseenter', () => {
                clearTimeout(anim);
                if(e.getAttribute('thumb-src') !== thumb.src){
                    anim = setTimeout(() => {
                        gsap.to(thumb, 0.35, {opacity: 0});
                        setTimeout(() => {
                            thumb.src = e.getAttribute('thumb-src');
                            gsap.to(thumb, 0.35, {opacity: 1});
                        }, 350);
                    }, 350);
                }
            });
        });
    }
    else{
        console.warn('No links found: make sure function is called on the right page.');
    }
}

let listener = null;
function AnchorScrollSpy(){
    let anchors = Array.from(document.querySelectorAll('.single__anchors a')),
    target = anchors.map(a => { return document.querySelector('#'+a.href.split('#')[1]) }),     
    tracker_step = 1 / anchors.length,
    tracker = document.querySelector('.single__tracker');
    listener = GetVisibleAnchor;

    if(anchors.length !== 0){
        window.addEventListener('scroll', listener);
   
        document.querySelectorAll('.single__anchors a').forEach(e => {
            e.addEventListener('click', function (event){
                event.preventDefault();
                let scrollTo = document.querySelector(this.getAttribute('href'));
                scrollTo.scrollIntoView({
                    behavior: 'auto',
                    block: 'center',
                    inline: 'center'
                });
            });
        });

        if(tracker){
            anchors[0].classList.add('t');
            gsap.to(tracker, {duration: 0.35, scaleY: tracker_step});
        }
    }
    else{
        console.warn('No anchors found: make sure function is called on the right page."')
    }

    function changeTarget(ca, ind){
        if(ind === -1) { ind = 0; ca = document.querySelector('.single__anchors a'); }
        document.querySelectorAll('.single__anchors .t').forEach(e => e.classList.remove('t'));
        ca.classList.add('t');
        gsap.to(tracker, {duration: 0.35, scaleY: tracker_step * (ind+1)});
    }

    function GetVisibleAnchor() {
        let vh = window.innerHeight,
        lastId = "",
        fromTop = this.scrollY,
        cur = target.filter((t) => t.getBoundingClientRect().y + fromTop < fromTop + vh / 2);
        cur = cur[cur.length-1];
        let id = cur !== undefined ? cur.id : "";
        if(id !== lastId){
            lastId = id;
            let currentAnchor = document.querySelector('.single__anchors a[href="#'+id+'"]');
            changeTarget(currentAnchor, anchors.indexOf(currentAnchor));
        }
    }
}

function removeScrollListener(){
    window.removeEventListener('scroll', listener);
}

function GetBlockAssets(block, js = false){
    return new Promise(resolve => {
        if(!callback[block]) callback[block] = new Promise((res, rej) => deffered[block] = { resolve: res, reject: rej });
        if(!document.querySelector(`#block-acf-${block}-css`) && !document.querySelector(`#block-acf-${block}-js`)){
            fetch(ajaxurl + '?action=assetsLoad&block='+block)
                .then(data => data.json())
                .then(res => CreateAssets(res['data'], js).then((res) => resolve(res)))
                .catch(err => console.error(err));
        } else { 
            resolve(`Block ${block} already loaded`);
        }
    });
}

function CreateAssets(data, js){
    const head = document.querySelector('head');
    return new Promise(async resolve => {
        if(!document.querySelector(`#block-acf-${data['name']}-css`)){
            const style = document.createElement('link');
            Object.assign(style, {
                rel: 'stylesheet',
                href: data['css'],
                type: 'text/css',
                id: 'block-acf-' + data['name'] + '-css',
            });
            head.appendChild(style);
        }
        if(js && !document.querySelector(`#block-acf-${data['name']}-js`)){
            const script = document.createElement('script');
            Object.assign(script, {
                src: data['js'],
                id: 'block-acf-' + data['name'] + '-js',
                type: 'text/javascript'
            });
            head.appendChild(script);
        }
        resolve(`Block assets loaded: ${data['name']}`);
    })
}

function StickyHeaderListener(){
    window.addEventListener('scroll', () => {
        if(window.scrollY < 150){
            document.body.classList.remove('header-sticky');
        }
        else{
            document.body.classList.add('header-sticky');
        }
    });
}

function ToggleLockSticky(state){
    state ? document.body.classList.add('sticky-lock') : document.body.classList.remove('sticky-lock');
}

function ToggleInvertFilter(e){
    e.classList.toggle('invert');
}

function ToggleNavVisibility(){
    document.querySelector('nav .burger-nav').classList.toggle('open');
    document.body.classList.toggle('nav-open');
}

function ListenForBurgerMenu(){
    const burger = document.querySelector('.burger');
    burger.addEventListener('click', ToggleNavVisibility);
}

function ListenForNavLink(){
    let anim = null;
    const navLinks = document.querySelectorAll(".burger-nav a[data-src]");
    navLinks.forEach(l => l.addEventListener('mouseenter', () => {
        const target = document.querySelector('.nav-img');
        if(target.src !== l.getAttribute('data-src')){
            clearTimeout(anim);
            target.style.opacity = 0;
            anim = setTimeout(() => {
                target.src = l.getAttribute('data-src');
                target.style.opacity = 1;
            }, 350);
        }
    }));
}

function ListenForBigTitle(){
    let elements = document.querySelectorAll('.resize');
    if(elements.length !== 0){
        elements.forEach(e => {
            if(window.innerWidth > 768){
                gsap.to(e, 1, {fontSize: '7vw', letterSpacing: '5px', opacity: 0.4, ease: Power1.easeOut,
                scrollTrigger: {
                    trigger: e,
                    scrub: true,
                    start: "top center+=300px",
                    end: "bottom center"
                }});
            }
            else{
                gsap.to(e, 1, {fontSize: '30px', letterSpacing: '2px', opacity: 0.4, ease: Power1.easeOut,
                scrollTrigger: {
                    trigger: e,
                    scrub: true,
                    start: "top center+=300px",
                    end: "bottom center",
                }});
            }
        });
    }
    else{
        console.warn('No elements found to animate: make sure function is called on the right page');
    }
}

function HideFooter(){
    document.querySelector('footer.site-footer').classList.add('footer-hide');
}

function ShowFooter(){
    document.querySelector('footer.site-footer').classList.remove('footer-hide');
}

function ListenForToggleSideBar(){
    if(window.innerWidth < 768){
        const tab = document.querySelector('.single__sidebar_tab');
        const sidebar = document.querySelector('.single__sidebar');
        let isDragging = false;
        let p = 100;
        tab.addEventListener('click', () => {
            sidebar.classList.toggle('open')
            sidebar.classList.contains('open') ? sidebar.style.transform = `translateX(0%)` : sidebar.style.transform = `translateX(100%)` ;
        });


        function Dragging(e){
            isDragging = true;
            let fX = Math.floor(e.touches[0].clientX);
            if(fX > 0 && fX < window.innerWidth){
                p = Math.floor(fX/window.innerWidth*100);
                sidebar.style.transform = `translateX(${p}%)`;
            }
        }

        function ReleaseDrag(e){
            if(isDragging){
                if(p > 50){
                    sidebar.classList.remove('open');
                    sidebar.style.transform = `translateX(100%)`;
                }
                else{
                    sidebar.classList.add('open');
                    sidebar.style.transform = `translateX(0%)`;
                }
                isDragging = false;
            }
        }

        tab.addEventListener('touchstart', () => document.addEventListener('touchmove', Dragging));
        document.addEventListener('touchend', () => document.removeEventListener('touchmove', Dragging));
        document.addEventListener('touchend', ReleaseDrag);
    }
}


docReady(function (){
    StickyHeaderListener();
    ListenForBurgerMenu();
    if(window.innerWidth > 768) ListenForNavLink();
    gsap.registerPlugin(ScrollTrigger);
    // Preload blocks assets
    setTimeout(() => {
        GetBlockAssets('homepage', true);
        GetBlockAssets('presentation', true);
    }, 1000);
});